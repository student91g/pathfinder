﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class GameLoaded : MonoBehaviour
{
    static GameLoaded() {
        PlayerPrefs.SetInt("Level", 1);
    }
}
