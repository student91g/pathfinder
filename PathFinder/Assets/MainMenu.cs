﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    private void Start()
    {
        GameObject playButton = GameObject.FindGameObjectWithTag("StartGame");
        int level = PlayerPrefs.GetInt("Level");
        if (level == 1)
        {
            playButton.GetComponentInChildren<TextMeshProUGUI>().text = "Start Game";
        }
        else
        {
            playButton.GetComponentInChildren<TextMeshProUGUI>().text = "Start Lvl"+level;
        }
    }
    public void PlayGame()
    {
        int levelNumber = PlayerPrefs.GetInt("Level");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        PlayerPrefs.SetInt("Level", ++levelNumber);
    }
    public void QuitGame() {
        Debug.Log("Quit!");
        Application.Quit();
    }
}
