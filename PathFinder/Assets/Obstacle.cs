﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    private Vector2 _screenBounds;
    public GameObject obstaclePrefab;
    public float respawnTime = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 position = new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z);
        _screenBounds = Camera.main.ScreenToWorldPoint(position);
        ObstacleWave();
    }
    private void SpawnObstacle() {
        if (obstaclePrefab != null)
        {
            GameObject a = Instantiate(obstaclePrefab) as GameObject;
            a.transform.parent = GameObject.Find("Grid").transform;
            int x = (int)Random.Range(-_screenBounds.x, _screenBounds.x);
            int y = (int)Random.Range(-_screenBounds.y, _screenBounds.y);
            a.transform.position = new Vector2(x+0.5f, y+0.5f);
            a.layer = 8;
            a.GetComponent<Renderer>().sortingOrder = 1;
        }
    }
    // Update is called once per frame
    void Update()
    {
        //Destroy(this.gameObject);
    }
    void ObstacleWave() {
        int numberOfObjects = PlayerPrefs.GetInt("Level");
        while (numberOfObjects>0)
        {
            SpawnObstacle();
            numberOfObjects = numberOfObjects-1;
        }
    }
}
