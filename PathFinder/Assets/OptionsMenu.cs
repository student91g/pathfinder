﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OptionsMenu : MonoBehaviour
{
    public Camera Camera;
    private Dictionary<int, int> _valueNumber;
    private void Start()
    {
        _valueNumber = new Dictionary<int, int>();
        _valueNumber.Add(0,-4);
        _valueNumber.Add(1,-3);
        _valueNumber.Add(2,-2);
        _valueNumber.Add(3,-1);
        _valueNumber.Add(4, 0);
        _valueNumber.Add(5, 1);
        _valueNumber.Add(6, 2);
        _valueNumber.Add(7, 3);
        _valueNumber.Add(8, 4);
    }
    public void SetMapSize(float size) 
    {
        if (Camera)
        {
            Camera.orthographic = true;
            Camera.orthographicSize = size;
        }
        
    }
    public void SetFullScreen(bool isFullscreen) 
    {
        Screen.fullScreen = isFullscreen;
    }
    public void setXStartCoord(int value) {
        int xStartValue = _valueNumber[value];
        PlayerPrefs.SetFloat("xStart",xStartValue+0.5f);
    }
    public void setXEndCoord(int value)
    {
        int xEndValue = _valueNumber[value];
        PlayerPrefs.SetFloat("xEnd", xEndValue + 0.5f);
    }
    public void setYStartCoord(int value)
    {
        int yStartValue = _valueNumber[value];
        PlayerPrefs.SetFloat("yStart", yStartValue + 0.5f);
        //GameObject start = GameObject.FindGameObjectsWithTag("Player").FirstOrDefault();
        //start.transform.position = new Vector2(start.transform.position.x,yStartValue + 0.5f);
        //GameObject startPost = GameObject.FindGameObjectsWithTag("Start").FirstOrDefault();
        //startPost.transform.position = new Vector2(start.transform.position.x, yStartValue + 0.5f);
    }
    public void setYEndCoord(int value)
    {
            int yEndValue = _valueNumber[value];
            PlayerPrefs.SetFloat("yEnd", yEndValue + 0.5f);
    }

}
