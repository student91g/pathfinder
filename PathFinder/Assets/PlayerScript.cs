﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float x = PlayerPrefs.GetFloat("xStart");
        float y = PlayerPrefs.GetFloat("yStart");
        this.gameObject.transform.position = new Vector2(x, y);
    }
}
