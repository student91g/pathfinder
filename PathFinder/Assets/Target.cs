﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float x = PlayerPrefs.GetFloat("xEnd");
        float y = PlayerPrefs.GetFloat("yEnd");
        this.gameObject.transform.position = new Vector2(x,y);
    }

}
